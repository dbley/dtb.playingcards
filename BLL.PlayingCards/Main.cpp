// C++ Assignment 3 Playing Cards
// Bret L Lewis

#include <iostream>
#include <conio.h>
#include <string>


using namespace std;

enum Rank
{
	TWO = 2,
	THREE,
	FOUR, 
	FIVE,
	SIX,
	SEVEN, 
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum Suit
{
	HEARTS,
	DIAMONDS,
	SPADES,
	CLUBS
};

struct Card
{
	Suit suit;
	Rank rank;
};

//Function prototypes.
void PrintCard(Card card);
Suit GetSuit();
Rank GetRank();
Card HighCard(Card card1, Card card2);

//START OF APPLICATION.
int main()
{
	Card card1;

	card1.suit = GetSuit();
	card1.rank = GetRank();

	PrintCard(card1);

	Card card2;

	card2.suit = GetSuit();
	card2.rank = GetRank();

	PrintCard(card2);

	Card high = HighCard(card1, card2);
	
	cout << "The higher ranked card is..." << "\n";

	PrintCard(high);

	_getch();
	return 0;
}

//Function to ask for suit.
Suit GetSuit()
{
	cout << "Please enter a number between 0 and 3: ";
	int input;
	cin >> input;
	while (input < 0 || input > 3)
	{
		cout << "Invalid entry. Please enter a number between 0 and 3: ";
		cin >> input;
	}

	Suit suit = HEARTS;
	switch(input)
	{
	case 1: suit = DIAMONDS;
		break;
	case 2: suit = SPADES;
		break;
	case 3: suit = CLUBS;
	}

	return suit;
}

//Fuction to ask for rank.
Rank GetRank()
{
	cout << "Please enter a number between 2 and 14: ";
	int input;
	cin >> input;
	while (input < 2 || input > 14)
	{
		cout << "Invalid entry. Please enter a number between 2 and 14 ";
		cin >> input;
	}
	Rank rank = TWO;
	switch (input)
	{
	case 3: rank = THREE;
		break;
	case 4: rank = FOUR;
		break;
	case 5: rank = FIVE;
		break;
	case 6: rank = SIX;
		break;
	case 7: rank = SEVEN;
		break;
	case 8: rank = EIGHT;
		break;
	case 9: rank = NINE;
		break;
	case 10: rank = TEN;
		break;
	case 11: rank = JACK;
		break;
	case 12: rank = QUEEN;
		break;
	case 13: rank = KING;
		break;
	case 14: rank = ACE;
	}

	return rank;
}

//Print card function using user suit and rank input.
void PrintCard(Card card)
{
	string rank;
	switch (card.rank)
	{
	case TWO: rank = "Two";
		break;
	case THREE: rank = "Three";
		break;
	case FOUR: rank = "Four";
		break;
	case FIVE: rank = "Five";
		break;
	case SIX: rank = "Six";
		break;
	case SEVEN: rank = "Seven";
		break;
	case EIGHT: rank = "Eight";
		break;
	case NINE: rank = "Nine";
		break;
	case TEN: rank = "Ten";
		break;
	case JACK: rank = "Jack";
		break;
	case QUEEN: rank = "Queen";
		break;
	case KING: rank = "King";
		break;
	case ACE: rank = "Ace";
	}

	string suit;
	switch (card.suit)
	{
	case HEARTS: suit = "Hearts";
		break;
	case DIAMONDS: suit = "Diamonds";
		break;
	case SPADES: suit = "Spades";
		break;
	case CLUBS: suit = "Clubs";
	}

	cout << "\n" << "     The " << rank << " of " << suit << "\n\n";

}

//High card function.
Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	else if (card2.rank > card1.rank)
	{
		return card2;
	}
	else
	{
		cout << "Your card ranks are equal.";
	}

}